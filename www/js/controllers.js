angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {

  $scope.data ={};
  var licenseiOs = "HFMCK3F6-FHLVEYIR-C6FPDOKG-FOOMMKOS-BIRG6AIZ-N4ARS3YB-DFXQCGLP-AEMT4NT4";
  // This license is only valid for package name "mobi.pdf417.demo"
  var licenseAndroid = "MHZXNXML-QXBQHAQO-OX4FAM3Y-HOAXHYW6-ICXC25DI-FKQFAM7Y-LQUEKNQV-6CAHZP5T";
  // This license is only valid for Product ID "e2994220-6b3d-11e5-a1d6-4be717ee9e23"
  var licenseWP8 = "5JKGDHZK-5WN4KMQO-6TZU3KDQ-I4YN67V5-XSN4FFS3-OZFAXHK7-EMETU6XD-EY74TM4T";
  //
  var types = ["PDF417", "QR Code"];
  /**
  * Initiate scan with options
  * NOTE: Some features are unavailable without a license
  * Obtain your key at http://pdf417.mobi
  */
  var options = {
    beep : true,  // Beep on
    noDialog : true, // Skip confirm dialog after scan
    uncertain : false, //Recommended
    quietZone : false, //Recommended
    highRes : false, //Recommended
    inverseScanning: false,
    frontFace : false
  };

  $scope.escanear = function(){

    cordova.plugins.pdf417Scanner.scan(function callback(scanningResult) {
 		// handle cancelled scanning
    //  (scanningResult.cancelled == true) {
    //      sultDiv.innerHTML = "Cancelled!";
    //      return;
		// 	}
		// Obtain list of recognizer results
      var resultList = scanningResult.resultList;
      alert(resultList[0].data);
      $scope.data.resultado = resultList[0].data;
		},
		// Register the error callback
		function errorHandler(err) {
		    alert('Error');
	  },
      types, options, licenseiOs, licenseAndroid
		);
  }
})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
